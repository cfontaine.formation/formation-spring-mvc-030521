package fr.dawan.springcore.components;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("helloWorld")
@Scope("prototype")
public class HelloWorldComponent {
    
    public void afficher() {
        System.out.println("Hello world");
    }
    
    

}
