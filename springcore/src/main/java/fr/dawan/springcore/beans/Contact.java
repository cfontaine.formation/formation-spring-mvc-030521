package fr.dawan.springcore.beans;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class Contact implements Serializable {

    private static final long serialVersionUID = 1L;

    private String prenom;
    private String nom;
    
    @Autowired
    @Qualifier("adresse1")
//    @Inject
//    @Named("adresse1")
    private Adresse adresse;
    
    private List<Telephone> telephones;

    public Contact() {
    }

    
    public Contact(String prenom, String nom, Adresse adresse) {
        System.out.println("Constructeur Contact (3 paramètres)");
        this.prenom = prenom;
        this.nom = nom;
        this.adresse = adresse;
    }

    public Contact(String prenom, String nom, Adresse adresse, List<Telephone> telephones) {
        this.prenom = prenom;
        this.nom = nom;
        this.adresse = adresse;
        this.telephones = telephones;
    }

    public Contact(Adresse adresse) {
        System.out.println("Constructeur Contact (un paramètre Adresse)");
        this.adresse = adresse;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public List<Telephone> getTelephones() {
        return telephones;
    }

    public void setTelephones(List<Telephone> telephones) {
        this.telephones = telephones;
    }

    
    public void setAdresse(Adresse adresse) {
        System.out.println("Setter Adresse Contact");
        this.adresse = adresse;
    }
    
    @PostConstruct
    public void init() {
        System.out.println("Methode init");
    }
    
    @PreDestroy
    public void destroy() {
        System.out.println("Methode destroy");
    }

    @Override
    public String toString() {
        String tmp = "Contact [prenom=" + prenom + ", nom=" + nom + ", adresse=" + adresse + ", toString()="
                + super.toString() + "]";
        if (telephones != null) {
            for (Telephone s : telephones) {
                tmp += "  " + s.getNumero();
            }
        }
        return tmp;
    }

}
