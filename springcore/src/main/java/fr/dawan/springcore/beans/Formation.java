package fr.dawan.springcore.beans;

import java.io.Serializable;

public class Formation implements Serializable {


    private static final long serialVersionUID = 1L;
    
    private String intitule;
    private int duree;
    private double prix;
    private String description;

    public Formation() {
        System.out.println("Constructeur par défaut Formation");
    }

    public Formation(String intitule, int duree, double prix, String description) {
       System.out.println("Constructeur 4 paramètres Formation");
        this.intitule = intitule;
        this.duree = duree;
        this.prix = prix;
        this.description = description;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        System.out.println("Setter Formation Intitutlé");
        this.intitule = intitule;
    }

    public int getDuree() {
        return duree;
    }

    public void setDuree(int duree) {
        this.duree = duree;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Formation [intitule=" + intitule + ", duree=" + duree + ", prix=" + prix + ", description="
                + description + ","+super.toString()+"]";
    }
    
    public void initialisation() {
        System.out.println("Méthode init");
    }

    public void destruction() {
        System.out.println("Méthode destroy");
    }
}
