package fr.dawan.springcore.beans;

public class Client extends Contact {


    private static final long serialVersionUID = 1L;
    
    private String numero;

    public Client() {
    }

    public Client(String prenom, String nom, Adresse adresse,String numero) {
        super(prenom, nom, adresse);
        this.numero=numero;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Override
    public String toString() {
        return "Client [numero=" + numero + ", toString()=" + super.toString() + "]";
    }

}
