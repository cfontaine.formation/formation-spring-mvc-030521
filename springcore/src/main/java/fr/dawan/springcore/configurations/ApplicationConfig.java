package fr.dawan.springcore.configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import fr.dawan.springcore.beans.Adresse;
import fr.dawan.springcore.beans.Contact;
import fr.dawan.springcore.beans.Formation;

@Configuration
@ComponentScan(basePackages = "fr.dawan.springcore")
public class ApplicationConfig {

    @Bean(name= {"formation1","formationJava"}, initMethod = "initialisation", destroyMethod = "destruction")
    @Scope("prototype")
    public Formation formation1()
    {
        return new Formation("formation Java",35,1000.0,"description formation");
    }
    
    @Bean
    public Adresse adresse1() {
        return new Adresse("1, rue esquermoise","lille","59800");
    }
    
    @Bean
    public Contact contact1(Adresse adr) {
        return new Contact("john","doe",adr);
    }
    
    @Bean
    public Contact contact2() {
        return new Contact("john","doe",adresse1());
    }
}
