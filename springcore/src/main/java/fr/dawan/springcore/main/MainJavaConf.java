package fr.dawan.springcore.main;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import fr.dawan.springcore.beans.Contact;
import fr.dawan.springcore.beans.Formation;
import fr.dawan.springcore.components.HelloWorldComponent;
import fr.dawan.springcore.configurations.ApplicationConfig;

public class MainJavaConf {
    
    public static void main(String []args) {
        AnnotationConfigApplicationContext ctx=new AnnotationConfigApplicationContext(); //ApplicationConfig.class
        ctx.register(ApplicationConfig.class);
        ctx.refresh();
        Formation f1=ctx.getBean("formation1",Formation.class);
        System.out.println(f1);
        Formation f2=ctx.getBean("formation1",Formation.class);
        System.out.println(f2);
        HelloWorldComponent hw=ctx.getBean("helloWorld",HelloWorldComponent.class);
        hw.afficher();
        
        Contact c1 = ctx.getBean("contact1",Contact.class);
        System.out.println(c1);
        
        Contact c2 = ctx.getBean("contact2",Contact.class);
        System.out.println(c2);
        
        ctx.close();
    }

}
