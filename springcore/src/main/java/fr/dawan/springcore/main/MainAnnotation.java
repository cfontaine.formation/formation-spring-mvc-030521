package fr.dawan.springcore.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.dawan.springcore.beans.Contact;
import fr.dawan.springcore.components.HelloWorldComponent;

public class MainAnnotation {

    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("annotationConf.xml");
        HelloWorldComponent hw = ctx.getBean("helloWorld", HelloWorldComponent.class);
        hw.afficher();
        Contact c1 = ctx.getBean("contact1", Contact.class);
        System.out.println(c1);

        HelloWorldComponent hw1 = ctx.getBean("helloWorld", HelloWorldComponent.class);
        System.out.println(hw1);
        HelloWorldComponent hw2 = ctx.getBean("helloWorld", HelloWorldComponent.class);
        System.out.println(hw2);
   
    
        ((AbstractApplicationContext) ctx).close();
    }

}
