package fr.dawan.springcore.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.dawan.springcore.beans.Client;
import fr.dawan.springcore.beans.Contact;
import fr.dawan.springcore.beans.Formation;

public class MainXml {

    public static void main(String[] args) {
        // Création du conteneur
        ApplicationContext context = new ClassPathXmlApplicationContext(new String[] { "beans.xml", "adresses.xml" });
        System.out.println("_______________________________________________");
    
        // getBean permet de récupérer les instances des beans depuis le conteneur
        Formation f1 = context.getBean("formationJava", Formation.class);
        System.out.println(f1);
        Formation f2 = context.getBean("formationExcel", Formation.class);
        System.out.println(f2);

        // Porté (Scope)
        // Singleton
        // pour un singleton (ex: formationJava) getBean("formationJava") retourne
        // toujours la même instance
        Formation f3 = context.getBean("formationJava", Formation.class);
        System.out.println(f1);
        System.out.println(f3);

        // Prototype
        // Pour un prototype (ex: formationWord) getBean("formationWord") retourne une
        // nouvelle instance à chaque appel
        // correspond à un new
        Formation f4 = context.getBean("formationWord", Formation.class);
        Formation f5 = context.getBean("formationWord", Formation.class);
        System.out.println(f4);
        System.out.println(f5);

        // Exercice Création de Bean
//        Adresse adr1=context.getBean("adresse1",Adresse.class);
//        Adresse adr2=context.getBean("adresse",Adresse.class);
//        System.out.println(adr1);
//        System.out.println(adr2);

        // Injection de dépendence explicite ou automatique
        Contact c1 = context.getBean("contact1", Contact.class);
        System.out.println(c1);
        Contact c2 = context.getBean("contact2", Contact.class);
        System.out.println(c2);
        Contact c3=context.getBean("contact3",Contact.class);
        System.out.println(c3);
        
        // Héritage
        Client cl1=context.getBean("client1",Client.class);
        System.out.println(cl1);
        
     //   Contact c4=context.getBean("contact4",Contact.class);
     //   System.out.println(c4);
        
        Client cl2=context.getBean("client2",Client.class);
        System.out.println(cl2);
        
        Client cl3=context.getBean("client3",Client.class);
        System.out.println(cl3);
        
        Client cl4=context.getBean("client4",Client.class);
        System.out.println(cl4);

        System.out.println("_______________________________________________");
        // Destruction du conteneur
        ((AbstractApplicationContext) context).close();
    }

}
