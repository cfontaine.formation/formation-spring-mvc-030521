package fr.dawan.springmvc.validators;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import fr.dawan.springmvc.forms.UserForm;

public class UserValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {

        return clazz == UserForm.class;
    }

    @Override
    public void validate(Object target, Errors errors) {
        UserForm formUser= (UserForm) target;
        if(formUser.getPassword()==null || formUser.getConfirmPassword()==null || !formUser.getPassword().equals(formUser.getConfirmPassword())){
            formUser.setConfirmPassword("");
            formUser.setPassword("");
            errors.rejectValue("confirmPassword", "user.confirmPassword.notequals","default message");
        }
    }

}
