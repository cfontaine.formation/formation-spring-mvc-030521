package fr.dawan.springmvc.forms;

import java.io.Serializable;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class LoginForm implements Serializable {
    
    private static final long serialVersionUID = 1L;

    @NotEmpty
    @Email
    private String email;
    
    @NotNull
    @Size(min=6,max=40)
    private String password;

    public LoginForm() {
    }

    public LoginForm(String email,  String password) {
        this.email = email;
        this.password = password;
    }
    
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }




}
