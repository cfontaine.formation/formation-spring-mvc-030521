package fr.dawan.springmvc.daos;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import fr.dawan.springmvc.entities.User;

public class FakeUserDao {

    private static List<User> users = new ArrayList<>();

    static {
        User u = new User("John", "Doe", "jod@dawan.com", LocalDate.of(1987, 5, 6));
        // User u=new User("John","Doe","jod@dawan.com",26);
        u.setId(1);
        u.setPassword("azerty");
        users.add(u);
        u = new User("Jane", "Doe", "jad@dawan.com", LocalDate.of(1991, 3, 16));
        // u=new User("Jane","Doe","jad@dawan.com",30);
        u.setId(2);
        u.setPassword("azerty");
        users.add(u);
        u = new User("Alan", "Smithee", "alan.smithee@dawan.com", LocalDate.of(1970, 10, 1));
        // u=new User("Alan","Smithee","alan.smithee@dawan.com",48);
        u.setId(3);
        u.setPassword("azerty");
        users.add(u);
    }
    static int cpt =3;

    public void saveOrUpdate(User u) {
        if (u.getId() == 0) {
            users.add(u);
            cpt++;
            u.setId(cpt);
        } else {
            for (int i = 0; i < users.size(); i++) {
                if (users.get(i).getId() == u.getId()) {
                    users.set(i, u);
                }
            }
        }
    }

    public void delete(User u) {
        users.remove(u);
    }

    public void delete(long id) {
        User u = find(id);
        users.remove(u);
    }

    public User find(long id) {
        for (User u : users) {
            if (u.getId() == id) {
                return u;
            }
        }
        return null;
    }

    public List<User> findAll() {
        return users;
    }

    public User findByEmailAndPassword(String email, String password) {

        for (User u : users) {
            if (email.equals(u.getEmail()) && password.equals(u.getPassword())) {
                return u;
            }
        }
        return null;
    }
    
    public User findByEmail(String email) {

        for (User u : users) {
            if (email.equals(u.getEmail())) {
                return u;
            }
        }
        return null;
    }

}
