package fr.dawan.springmvc.daos;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.springmvc.entities.User;

@Transactional
public class UserDao {
    
    @PersistenceContext
    EntityManager em;
    
    public void saveOrUpdate(User u) {
        if(u.getId()==0) {
            em.persist(u);
        }
        else {
            em.merge(u);
        }
    }

    public void delete(User u) {
        em.remove(u);
    }
    
    public void delete(long id)
    {
        User u=em.find(User.class, id);
        em.remove(u);
    }
    
    @Transactional(readOnly = true)
    public User find(long id) {
        return em.find(User.class, id);
    }
    
    @Transactional(readOnly = true)
    public List<User> findAll() {
       TypedQuery<User> query= em.createQuery("SELECT u FROM User u", User.class);
       return query.getResultList();
    }

    @Transactional(readOnly = true)
    public User findByEmailAndPassword(String email, String password) {
        TypedQuery<User> query= em.createQuery("SELECT u FROM User u WHERE u.email=:email AND u.password=:password", User.class);
        query.setParameter("email", email);
        query.setParameter("password", password);
        return query.getSingleResult();
    }
    
    @Transactional(readOnly = true)
    public User findByEmail(String email) {
        try {
        TypedQuery<User> query= em.createQuery("SELECT u FROM User u WHERE u.email=:email", User.class);
        query.setParameter("email", email);
        return query.getSingleResult();
        }
        catch(NoResultException e) {
            return null;
        }
    }
}
