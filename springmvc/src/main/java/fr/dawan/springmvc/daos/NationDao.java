package fr.dawan.springmvc.daos;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.transaction.annotation.Transactional;

import fr.dawan.springmvc.entities.Nation;
import fr.dawan.springmvc.entities.User;

@Transactional
public class NationDao {
    
    @PersistenceContext
    EntityManager em;
    
    public void saveOrUpdate(Nation u) {
        if(u.getId()==0) {
            em.persist(u);
        }
        else {
            em.merge(u);
        }
    }

    public void delete(Nation u) {
        em.remove(u);
    }
    
    public void delete(long id)
    {
        Nation u=em.find(Nation.class, id);
        em.remove(u);
    }
    
    @Transactional(readOnly = true)
    public Nation find(long id) {
        return em.find(Nation.class, id);
    }
    
    @Transactional(readOnly = true)
    public List<Nation> findAll() {
       TypedQuery<Nation> query= em.createQuery("SELECT u FROM Nation u", Nation.class);
       return query.getResultList();
    }
}