package fr.dawan.springmvc.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import fr.dawan.springmvc.daos.UserDao;
import fr.dawan.springmvc.entities.User;

@Controller
@RequestMapping("/admin")
public class AdminController {
    
    @Autowired 
    private UserDao userDao;
    
    @GetMapping("/users")
    public String displayUser(Model model)
    {
        List<User> users=userDao.findAll();
        model.addAttribute("users",users);
        return "user";
    }
    
    @GetMapping("/user/delete/{id}")
    public String deleteUser(@PathVariable int  id)
    {
        userDao.delete(id);
        return "redirect:/admin/users";
    }
    

}
