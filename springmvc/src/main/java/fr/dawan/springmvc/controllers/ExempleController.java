package fr.dawan.springmvc.controllers;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import fr.dawan.springmvc.daos.UserDao;
import fr.dawan.springmvc.entities.User;

@Controller
@RequestMapping("/exemple")
@SessionAttributes("user") // Methode2
public class ExempleController {
    
//    @Autowired
//    Provider<User> userProvider;

    @Autowired
    UserDao userDao;
    
    @Autowired
    JavaMailSender mailSender;
    
    @GetMapping(value = { "" })
    public String exemple() {
        return "exemple";
    }

    // @RequestMapping(value={"/helloworld","/hello"}, method= {RequestMethod.GET})
    @GetMapping(value = { "/helloworld", "/hello" })
    public String helloWorld() {
        return "helloworld";
    }

    // Model
    @GetMapping("/testmodel")
    public String testModel(Model model) {
        model.addAttribute("msg", "Test Model");
        model.addAttribute("i", -42);
        return "exemple";
    }

    @GetMapping("/testmodelandview")
    public ModelAndView testModelAndView() {
        ModelAndView mdv = new ModelAndView();
        mdv.setViewName("exemple");
        mdv.addObject("msg", "Test ModelAndView");
        return mdv;
    }

    // params
    @GetMapping(value = "/testparams", params = "id=42")
    public String testParam(Model model) {
        model.addAttribute("msg", "La requete contient un paramètre id");
        return "exemple";
    }

    // headers
    @GetMapping(value = "/testheaders", headers = "val=dawan")
    public String testHeader(Model model) {
        model.addAttribute("msg", "La requete contient un header val = dawan");
        return "exemple";
    }

    // PathVariable
    @GetMapping("/testpath/{id}")
    public String testPathVariable(@PathVariable String id, Model model) {
        model.addAttribute("msg", "@PathVariable = " + id);
        return "exemple";
    }

    @GetMapping("/testpath/{id}/action/{action}")
    public String testPathVariableMulti(@PathVariable String id, @PathVariable String action, Model model) {
        model.addAttribute("msg", "@PathVariable id= " + id + " action=" + action);
        return "exemple";
    }

    @GetMapping("/testpathmap/{id}/action/{action}")
    public String testPathVariableMulti(@PathVariable Map<String, String> map, Model model) {
        model.addAttribute("msg", "@PathVariable id= " + map.get("id") + " action=" + map.get("action"));
        return "exemple";
    }

    //

    @GetMapping("/testpathambigue/{id:[0-9]+}")
    public String testpathVariablePb1(@PathVariable String id, Model model) {
        model.addAttribute("msg", "@PathVariable id = " + id);
        return "exemple";
    }

    @GetMapping("/testpathambigue/{name:[a-z]+}")
    public String testpathVariablePb2(@PathVariable String name, Model model) {
        model.addAttribute("msg", "@PathVariable name = " + name);
        return "exemple";
    }

//    @GetMapping(value="/testpathoption/{id}")
//    public String testPathVariableOptionnel(@PathVariable(required=false) String id,Model model)
//    {
//        model.addAttribute("msg", "@PathVariable = " + id);
//        return "exemple";
//    }

    @GetMapping("/testrequestparam")
    public String testRequestParam(@RequestParam String id, Model model) {
        model.addAttribute("msg", "@RequestParam id=" + id);
        return "exemple";
    }

    @GetMapping(value = "/testparamambigue", params = "id")
    public String testRequestParamAmbigueId(@RequestParam String id, Model model) {
        model.addAttribute("msg", "@RequestParam id=" + id);
        return "exemple";
    }

    @GetMapping(value = "/testparamambigue", params = "name")
    public String testRequestParamAmbigueName(@RequestParam String name, Model model) {
        model.addAttribute("msg", "@RequestParam name=" + name);
        return "exemple";
    }

    @GetMapping("/testparamdefaut")
    public String testRequestParamDefaut(@RequestParam(defaultValue = "0") String id, Model model) {
        model.addAttribute("msg", "@RequestParam id=" + id);
        return "exemple";
    }

    @GetMapping("/testparamconv")
    public String testRequestParamConv(@RequestParam(defaultValue = "0") int id,
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @RequestParam("startdate") LocalDate startDate,
            Model model) {
        model.addAttribute("msg", "Test Conversion d'un paramètre id=" + id + "  startDate=" + startDate);
        return "exemple";
    }

    @GetMapping("/testallheader")
    public String testAllHeaders(@RequestHeader HttpHeaders headers, Model model) {
        List<String> hparam = new ArrayList<>();
        Set<Entry<String, List<String>>> sheader = headers.entrySet();
        for (Entry<String, List<String>> entry : sheader) {
            String str = entry.getKey() + "= ";
            for (String v : entry.getValue()) {
                str += v + " ";
            }
            hparam.add(str);
        }
        model.addAttribute("lstHeader", hparam);
        return "exemple";
    }

    // Redirection
    @GetMapping("/testredirect")
    public String testRedirect() {
        return "redirect:/exemple/helloworld";
    }

    @GetMapping("/testforward")
    public String testRedirectForward() {
        return "forward:/exemple/helloworld";
    }

    
    @PostMapping("/testflash")
    public String testFlash(User user, RedirectAttributes redirAtt) {
        redirAtt.addFlashAttribute("userAdd", user);
        return "redirect:/exemple/cibleflash";
    }

    @GetMapping("/cibleflash")
    public String testflashCible(@ModelAttribute("userAdd") User userAdd, Model model) {
        model.addAttribute("msg", "L'utilisateur " + userAdd.getPrenom() + " " + userAdd.getNom() + "est ajouté ");
        return "exemple";
    }
    
    
//  @ModelAttribute("current_time")
//  public void currentTime(Model model) {
//      System.out.println("ModelAttribute");
//      model.addAttribute("current_time", LocalDateTime.now());
//  }
    
    @ModelAttribute("current_time")
    public LocalDateTime currentTime() {
        return LocalDateTime.now();
    }

    @GetMapping("/genioexception")
    public String genIoException() throws IOException {
        throw new IOException("une IOexception");
    }

    @GetMapping("/genexception")
    public String genException() throws Exception {
        throw new Exception("une exception");
    }

    @ExceptionHandler(IOException.class)
    public String gestionIoException(IOException e, Model model) {
        model.addAttribute("trace", e.getStackTrace());
        model.addAttribute("msg", e.getMessage());
        return "exception";
    }

    @GetMapping("/writecookie")
    public String writeCookie(HttpServletResponse reponse) {
        Cookie cookie = new Cookie("test", "exemple");
        // cookie.setMaxAge(30);
        cookie.setHttpOnly(true);
        // cookie.setPath("/springmvc/presentation");
        reponse.addCookie(cookie);
        return "redirect:/exemple";
    }

    @GetMapping("/readcookie")
    public String readCookie(@CookieValue(value = "test", defaultValue = "defaulValue") String value, Model model) {
        model.addAttribute("msg", value);
        return "exemple";
    }

    // Méthode 1 JavaEE
    @GetMapping("/readsession1")
    public String readSession1(HttpServletRequest request, Model model) {
        HttpSession session = request.getSession();
        User u = (User) session.getAttribute("user");
        model.addAttribute("msg", u);
        return "exemple";
    }
    
    @GetMapping("/writesession1")
    public String writeSession1(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.setAttribute("user", new User("John","Doe","jo@dawan.com",LocalDate.now()));
        return "exemple";
    }

    // Méthode 2
    @GetMapping("/readsession2")
    public String readSession2( User u,Model model) {
        model.addAttribute("msg", u);
        return "exemple";
    }
    
    @GetMapping("/writesession2")
    public String writeSession2(Model model) {
        model.addAttribute("user", new User("Jane","Doe","jad@dawan.com",LocalDate.now()));
        return "exemple";
    } 

    @ModelAttribute("user")
    public User initUser() {
        System.out.println("ModelAttribute User");
        return new User();
    }
    
//    @GetMapping("/writesession3")
//    public String writeSession3(Model model) {
//        User u=userProvider.get();
//        u.setPrenom("John");
//        u.setNom("Doe");
//        return "exemple";
//    }
//    
//    
//    @GetMapping("/readsession3")
//    public String readsession3(Model model) {
//        User u=userProvider.get();
//        model.addAttribute("msg", u);
//        return "exemple";
//    }
    
    
    // Internationalisation/Theme
    @GetMapping("/intertheme")
    public String interTheme() {
        return "internationalisation";
    }
    
    
    // Upload 
    @PostMapping("/upload")
    public String uploadFile(@RequestParam("user-file") MultipartFile multipartFile, Model model) throws IOException {
        String nameFile=multipartFile.getOriginalFilename();
        try(BufferedOutputStream bow=new BufferedOutputStream(new FileOutputStream("C:\\Formations\\TestIO\\"+nameFile))) {
            bow.write(multipartFile.getBytes());
            bow.flush();
        }
        model.addAttribute("msg", "Téléchargement du fichier "+ nameFile);
        return "exemple";
    }
    
    // DownLoad
    @GetMapping("/download")
    public String downloadFile(HttpServletResponse reponse) throws IOException
    {
        reponse.setContentType("text/csv");
        reponse.setHeader("Content-Disposition", "attachment;filename=users.csv");
         ServletOutputStream o=reponse.getOutputStream();
        List<User> users=userDao.findAll();
        for(User u : users) {
            String tmp=String.join(";", Long.toString(u.getId()),u.getPassword(),u.getNom(),u.getEmail(),u.getDateNaissance().toString(),u.getPassword()).concat("\n");
            o.write(tmp.getBytes());
        }
        o.close();
        return "exemple";
    }
    
    // Envoie Email
    @GetMapping("/testmail")
    public String testMail()
    {
        SimpleMailMessage mailMsg=new SimpleMailMessage();
        mailMsg.setFrom("no-reply@dawan.com");
        mailMsg.setTo("chfontaine.formation@gmail.com");
        mailMsg.setSubject("Email de Test");
        mailMsg.setText("Le contenu de l'email");
        mailSender.send(mailMsg);
        return "redirect:/exemple";
    }

    @GetMapping("/testmailhtml")
    public String testMailHtml()
    {
        MimeMessagePreparator preparator=new MimeMessagePreparator() {

            @Override
            public void prepare(MimeMessage mimeMessage) throws Exception {
                mimeMessage.setFrom("jd@dawan.com");
                mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress("jad@dawan.com"));
                mimeMessage.setSubject("Email HTML de Test");
                mimeMessage.setContent("<h1> Le mail de test en HTML </h1>","text/html");
            }
            
        };
        mailSender.send(preparator);
        return "redirect:/exemple";
    }


}
