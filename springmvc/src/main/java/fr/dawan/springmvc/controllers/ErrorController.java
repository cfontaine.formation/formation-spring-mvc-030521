package fr.dawan.springmvc.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ErrorController {

    @RequestMapping("/pageerror")
    public String gestionErreur(HttpServletRequest request,Model model) {
        int code=(int) request.getAttribute("javax.servlet.error.status_code");
        model.addAttribute("codeErreur",code);
        switch(code) {
        case 404:
            model.addAttribute("msg","La page est introuvable"); 
            break;
        case 500:
            model.addAttribute("msg","Une erreur interne c'est produite"); 
            break;
        case 403:
            model.addAttribute("msg","vous n'avez pas l'autoristion de consulter cette page");
            break;
            default:
                model.addAttribute("msg","Une erreur c'est produite");
        }
        return "erreur";
    }
}
