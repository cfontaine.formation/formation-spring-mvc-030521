package fr.dawan.springmvc.controllers;

import javax.validation.Valid;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import fr.dawan.springmvc.daos.UserDao;
import fr.dawan.springmvc.entities.User;
import fr.dawan.springmvc.forms.LoginForm;
import fr.dawan.springmvc.forms.UserForm;
import fr.dawan.springmvc.validators.UserValidator;

@Controller
@SessionAttributes("isConnected")
public class LoginController {
    
    @Autowired 
    private UserDao userDao;
    
    @GetMapping("/login")
    public String loginGet(@ModelAttribute("formLogin") LoginForm formLogin) {
        return "login";
    }
    
    @PostMapping("/login")
    public ModelAndView login(@Valid @ModelAttribute("formLogin")LoginForm formLogin,BindingResult results) {
        ModelAndView mdv=new ModelAndView();
        if(results.hasErrors()) {
            mdv.setViewName("login");
            mdv.addObject("errors", results);
            mdv.addObject("formLogin",formLogin);
        }else {
          User user= userDao.findByEmail(formLogin.getEmail());
          if(user==null) {
              mdv.setViewName("login");
              mdv.addObject("formLogin",new LoginForm());
              mdv.addObject("msgerr","L'email et/ou le mot de passe ne sont pas correcte");
          }
          else if(BCrypt.checkpw(formLogin.getPassword(), user.getPassword())){
             mdv.addObject("isConnected", true);
             mdv.setViewName("redirect:/exemple");
          } else {
              mdv.setViewName("login");
              mdv.addObject("formLogin",new LoginForm());
              mdv.addObject("msgerr","L'email et/ou le mot de passe ne sont pas correcte");
          }
        }
        return mdv;
    }
    
    @GetMapping ("/logout")
    public String logout(Model model){
        model.addAttribute("isConnected", false);
        return "redirect:/exemple";
    }
    
    @GetMapping("/user/add")
    public String addUser(@ModelAttribute("formUser") UserForm formUser) {
        return "useradd";
    }

    @PostMapping("/user/add")
    public String addUser(@Valid @ModelAttribute("formUser") UserForm formUser,BindingResult result,Model model) {
       new UserValidator().validate(formUser, result);
        if(result.hasErrors()) {
            model.addAttribute("errors",result);
            model.addAttribute("formuser", formUser);
            return "useradd";
        }
        if(userDao.findByEmail(formUser.getEmail())!=null) {
            model.addAttribute("formuser", new UserForm());
            model.addAttribute("msgerr", "L'utilisateur existe déjà");
            return "useradd";
        }
        User user=new User(formUser.getPrenom(),formUser.getNom(),formUser.getEmail(),formUser.getDateNaissance());;
        user.setPassword(BCrypt.hashpw(formUser.getPassword(), BCrypt.gensalt()));
        userDao.saveOrUpdate(user);
        return "redirect:/exemple";
    }
    
    @ModelAttribute("isConnected")
    public boolean initIsConnected() {
        return false;
    }
}
