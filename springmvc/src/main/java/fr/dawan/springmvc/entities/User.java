package fr.dawan.springmvc.entities;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "user")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy =GenerationType.IDENTITY)
    private long id;
    
    @Version
    private int version;
    
    @Column(name = "prenom", length = 50)
    private String prenom;
   
    @Column(name = "nom", length = 50)
    private String nom;
    private String email;
   
    @Column(name = "password", length = 60)
    private String password;
   
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @Column(name = "date_naissance")
    private LocalDate dateNaissance;

    // private int age;
    public User() {
    }

    public User(String prenom, String nom, String email, LocalDate dateNaissance) {
        this.prenom = prenom;
        this.nom = nom;
        this.email = email;
        this.dateNaissance = dateNaissance;
    }

//    public User(String prenom, String nom, String email, int age) {
//        this.prenom = prenom;
//        this.nom = nom;
//        this.email = email;
//        this.age = age;
//    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LocalDate getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    @Override
    public String toString() {
        return "User [id=" + id + ", prenom=" + prenom + ", nom=" + nom + ", email=" + email + ", password=" + password
                + ", dateNaissance=" + dateNaissance + "]";
    }

//    public int getAge() {
//        return age;
//    }
//
//    public void setAge(int age) {
//        this.age = age;
//    }
//
//    @Override
//    public String toString() {
//        return "User [id=" + id + ", prenom=" + prenom + ", nom=" + nom + ", email=" + email + ", password=" + password
//                + ", age=" + age + "]";
//    }

}
