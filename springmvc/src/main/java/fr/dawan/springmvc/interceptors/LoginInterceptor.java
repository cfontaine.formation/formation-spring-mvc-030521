package fr.dawan.springmvc.interceptors;

import java.time.LocalDate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import fr.dawan.springmvc.entities.User;

public class LoginInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
       System.out.println("Interceptor pre Handle");
       HttpSession session=request.getSession();
       Boolean isConnected=(Boolean) session.getAttribute("isConnected");
       if(isConnected==null || !isConnected) {
           request.getRequestDispatcher("/login").forward(request, response);
           System.out.println("Non connecter");
           return false; 
       }
       System.out.println("Interceptor pre Handle");
       return true;
    }

//    @Override
//    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
//            ModelAndView modelAndView) throws Exception {
//        System.out.println("Interceptor postHandle");
//        HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
//    }
//
//    @Override
//    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
//            throws Exception {
//        System.out.println("Interceptor afterCompletion");
//        HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
//    }

    
}
