<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:import url="header.jsp">
    <c:param name="titre" value="login"/>
</c:import>

<c:url value="/login" context="/springmvc" var="urlLogin"/>
<form:form method="post" action="${urlLogin}" modelAttribute="formLogin" >
<h1 class="text-center my-5 offset-md-4 col-md-4"">Authentification</h1>
<div class="form-group my-5">
    <div class="mb-3 offset-md-4 col-md-4">
    <form:label class="form-label" path="email">Email</form:label>
    <form:input class="form-control" path="email" type="text" placeholder="Entrer votre email"/>
    <form:errors class="text-danger small" path="email"/>
    </div>
    <div class="mb-3 offset-md-4 col-md-4">
    <form:label class="form-label" path="password">Mot de passe</form:label>
    <form:input class="form-control" path="password" type="password" placeholder="Entrer votre mot de passe"/>
    <form:errors class="text-danger small" path="password"/>
    </div> 
    <input class="btn btn-primary my-5 offset-md-4 col-md-4" type="submit" value="Connexion"/>
    
</div>
</form:form>


<c:import url="footer.jsp"/>