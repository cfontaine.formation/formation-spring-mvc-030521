<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:import url="header.jsp">
    <c:param name="titre" value="Liste des utilisateurs"/>
</c:import>

<h1>Liste des utilisateurs</h1>
<table class="table table-striped table-hover">
<thead>
<tr>
<th>Prénom</th>
<th>Nom</th>
<th>Date Naissance</th>
<th>Email</th>
<th>Action</th>
</tr>
</thead>
<tbody>
<c:forEach items="${users}" var="u">
<tr>
<td><c:out value="${u.prenom}"/></td>
<td><c:out value="${u.nom}"/></td>
<td><c:out value="${u.dateNaissance}"/></td>
<td><c:out value="${u.email}"/></td>
<td><a class="btn btn-danger btn-sm" href='<c:url value="/admin/user/delete/${u.id}" context="/springmvc"/>'/>Supprimer</a></td>
</tr>
</c:forEach>
</tbody>
</table>



<c:import url="footer.jsp"/>