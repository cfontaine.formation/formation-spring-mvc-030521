<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:import url="header.jsp">
    <c:param name="titre" value="Internationalisation / Thème"/>
</c:import>

<h3 class="mb-5">Internationalisation</h3>

  <a href='<c:url value="/exemple/intertheme?lang=fr" context="/springmvc"/>' class="btn btn-primary mx-3"/>Français</a>
<a href='<c:url value="/exemple/intertheme?lang=en" context="/springmvc"/>' class="btn btn-primary mx-3"/>English</a>
<a href='<c:url value="/exemple/intertheme?lang=es" context="/springmvc"/>' class="btn btn-primary mx-3"/>Español </a>

<h3 class="my-5">Thème</h3>

<a href='<c:url value="/exemple/intertheme?theme=dark" context="/springmvc"/>' class="btn btn-secondary mx-3"/>Dark</a>
<a href='<c:url value="/exemple/intertheme?theme=blue" context="/springmvc"/>' class="btn btn-primary mx-3"/>Blue</a>
<a href='<c:url value="/exemple/intertheme?theme=red" context="/springmvc"/>' class="btn btn-danger mx-3"/>Rouge</a>
<c:import url="footer.jsp"/>