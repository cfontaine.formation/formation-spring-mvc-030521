<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:import url="header.jsp">
    <c:param name="titre" value="Exemple"/>
</c:import>
<h1>Exemples</h1>
<c:if test="${!empty current_time}">

<fmt:parseDate  value="${current_time}"  type="date" pattern="yyyy-MM-dd'T'HH:mm:ss." var="pcurrent_time" />
<fmt:formatDate value="${pcurrent_time}" type="both" var="strcurrent_time"/>

<div class="my-2"><c:out value="(${strcurrent_time})"/></div>
</c:if>
<c:if test="${not empty msg}">
<div class="alert alert-primary col-md-5"><c:out value="${msg}"/></div>
</c:if>
<c:if test="${!empty i && i>0}">
    <p>Supérieur à 0</p>
</c:if>

    <h4>Controleur</h4>
    <ul>
        <li><a class="my-2" href="<c:url value='/exemple/testmodel' context='/springmvc'/>">Model</a></li>
        <li><a class="my-2" href="<c:url value='/exemple/testmodelandview' context='/springmvc'/>">ModelAndView</a></li>
    </ul>
        
    <h4>RequestMapping</h4>
    <ul>
        <li>
            <c:url value="/exemple/testparams" context="/springmvc" var = "urlParams">
                <c:param name="id" value="42"/>
            </c:url>
            <a class="my-2" href="${urlParams}"> url avec un paramètre id=42</a>
         </li>
                 <li>
            <c:url value="/exemple/testparams" context="/springmvc" var = "urlParams">
                <c:param name="id" value="3"/>
            </c:url>
            <a class="my-2" href="${urlParams}"> url avec un paramètre id=3 (erreur)</a>
         </li>
    </ul>
    
    <h4>PathVariable</h4>
    <ul>
        <li>
            <a class="my-2" href='<c:url value="/exemple/testpath/42" context="/springmvc" />'>url avec /exemple/testpath/42</a>
        </li>
        <li>
            <a class="my-2" href='<c:url value="/exemple/testpath/42/action/afficher" context="/springmvc" />'>url avec /exemple/testpath/42/action/afficher</a>
        </li>
        <li>
            <a class="my-2" href='<c:url value="/exemple/testpathmap/42/action/afficher" context="/springmvc" />'>url avec /exemple/testpathmap/42/action/afficher</a>
        </li>
        <li>
            <a class="my-2" href='<c:url value="/exemple/testpathambigue/42" context="/springmvc" />'>url avec /exemple/testpathambigue/42</a>
        </li>
        <li>
            <a class="my-2" href='<c:url value="/exemple/testpathambigue/smithee" context="/springmvc" />'>url avec /exemple/testpathambigue/Smithee</a>
        </li>
    </ul>

    <h4>RequestParam</h4>
    <ul>
        <li>
            <a class="my-2" href='<c:url value="/exemple/testrequestparam?id=23" context="/springmvc" />'>test paramètre get id=23</a>
        </li>
        <li>
            <a class="my-2" href='<c:url value="/exemple//testparamdefaut?id=56" context="/springmvc" />'>test paramètre par défaut get id=56</a>
        </li>
         <li>
            <a class="my-2" href='<c:url value="/exemple/testparamdefaut" context="/springmvc" />'>test paramètre par défaut</a>
        </li>
             <li>
            <a class="my-2" href='<c:url value="/exemple/testparamconv?id=4&startdate=2021-05-05" context="/springmvc" />'>test de conversion de paramètre id=4 startdate=2021-05-05</a>
        </li>

    </ul>

    <h4>RequestHeader</h4>
    <ul>
        <li><a class="my-2" href='<c:url value="/exemple/testallheader" context="/springmvc" />'>Afficher tous les en-têtes</a></li>
        <c:if test="${! empty lstHeader}">
            <div class="alert alert-secondary">
            <c:forEach items="${lstHeader}" var="elm">
                <li><c:out value="${elm}"/></li>
            </c:forEach>

            </div>
        </c:if>
    </ul>
    
    <h4>Redirection</h4>
    <ul>
        <li><a class="my-2" href='<c:url value="/exemple/testredirect" context="/springmvc"/>'/>Test redirection(redirect)</a></li>
        <li><a class="my-2" href='<c:url value="/exemple/testforward" context="/springmvc"/>'/>Test redirection(forward)</a></li>
     </ul>
     
     <h4>Lien Bean et Flash Atrribute</h4>
     <form method="post" action="<c:url value='/exemple/testflash' context='/springmvc'/> ">
        <div class="form-group col-md-5">
        <div class="mb-3">
        <label for="prenom" class="form-label">Prénom</label>
        <input type="text" class="form-control col-md-4" id="prenom" name="prenom" placeholder="Entrer votre prénom"/>
        </div>
        <div class="mb-3">
        <label for="nom" class="form-label">Nom</label>
        <input type="text" class="form-control col-md-4" id="nom"  name="nom"  placeholder="Entrer votre nom"/>
        </div>
        <div class="mb-3">
        <label for="dateNaissance" class="form-label">Date de naisance</label>
        <input type="date" class="form-control col-md-4" id="dateNaissance"  name="dateNaissance"  placeholder="Entrer votre date de naissance"/>
        </div>
        <div class="mb-3">
        <label for="email" class="form-label">Email</label>
        <input type="email" class="form-control col-md-4" id="email"  name="email"  placeholder="Entrer votre email"/>
        </div>
        <div class="mb-3">
        <label for="password" class="form-label">Mot de passe</label>
        <input type="password" class="form-control col-md-4" id="password"  name="password"  placeholder="Entrer votre mot de passe"/>
        </div>
            <button class="btn btn-primary  col-md-5" type="submit">Ajouter</button>
        </div>
    </form>
    <h4>Upload</h4>
    <form action='<c:url value="/exemple/upload" context="/springmvc"/>' method="post" encType="multipart/form-data">
        <div class="form-group col-md-5">
	        <div class="mb-3">
	        <label for="user-file" class="form-label">Fichier</label>
	        <input type="file" class="form-control col-md-4" id="user-file" name="user-file"/>
	        </div>
	            <button class="btn btn-primary  col-md-5" type="submit">Upload</button>
        </div>
    </form>
    <h4>Download</h4>
    <a href='<c:url value="/exemple/download" context="/springmvc"/>' class="btn btn-primary">Télécharger</a>
    
    <h4>Email</h4>
    <ul>
    <li><a href='<c:url value="/exemple/testmail" context="/springmvc"/>'>Envoyer un email</a>
    <li><a href='<c:url value="/exemple/testmailhtml" context="/springmvc"/>'>Envoyer un email HTML</a>
    </ul>
<c:import url="footer.jsp"/>