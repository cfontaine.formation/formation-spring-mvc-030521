<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:import url="header.jsp">
    <c:param name="titre"  value="Exception"/>
</c:import>

<h1 class="text-center">Exception</h1>
<h3 class="text-center">Message: <c:out value="${msg}"/></h3>
<div class="text-danger">
<c:forEach items="${trace}" var="elm">
<c:out value="${elm}"/>
</c:forEach>
</div>
<c:import url="footer.jsp"/>