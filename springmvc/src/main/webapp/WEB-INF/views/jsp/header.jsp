<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <c:url value="/webjars/bootstrap/5.0.0-beta3/css/bootstrap.min.css" context="/springmvc" var="urlbootstrap"/>
    <link rel="stylesheet" href="${urlbootstrap}" />
    <title><c:out value='${empty param.titre?"Spring MVC":param.titre}'/></title>
</head>
<body>
    <main class="container-fluid">
        <nav class='navbar navbar-expand-lg sticky-top mb-4 <spring:theme code="background" text="bg-dark"/> navbar-dark  mb-4'>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <a class="mx-2 navbar-brand" href="/springmvc/exemple">Formation spring Mvc</a>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class = "navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href='<c:url value="/exemple" context="/springmvc"/>'>Exemple</a>
                </li>   
                <li class="nav-item">
                    <a class="nav-link" href='<c:url value="/presentation" context="/springmvc"/>'>Présentation</a>
                </li>             
				<li class="nav-item dropdown">
				    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
				    Exception
				    </a>
				    <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
				        <li><a class="dropdown-item" href="<c:url value="/exemple/genioexception" context="/springmvc"/>">Générer IOException</a></li>
				        <li><a class="dropdown-item" href="<c:url value="/exemple/genexception" context="/springmvc"/>">Générer Exception</a></li>
				        <li><a class="dropdown-item" href="<c:url value="/exemple/existepas" context="/springmvc"/>">Générer erreur 404</a></li>
				    </ul>
				</li>
				<li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Utilisateurs
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <li><a class="dropdown-item" href="<c:url value="/user/add" context="/springmvc"/>"><spring:message code="inscription" text="Inscription"/></a></li>
                        <li><a class="dropdown-item" href="<c:url value="/admin/users" context="/springmvc"/>">Administration</a></li>
                    </ul>
                </li>
                <li class="nav-item">
                   <a class="nav-link" href='<c:url value="/exemple/intertheme" context="/springmvc"/>'>Internationalisation/Théme</a>
                </li>    
            </ul>
               </div>
            <c:choose>
            <c:when test="${!empty sessionScope.isConnected && sessionScope.isConnected }">
                <a class="btn btn-primary mx-2" href='<c:url value="/logout" context="/springmvc"/>'/><spring:message code="logout" text="Logout"/></a>
            </c:when>
            <c:otherwise>
                <a class="btn btn-success mx-2" href='<c:url value="/login" context="/springmvc"/>'/><spring:message code="login" text="Login"/></a>
            </c:otherwise>
            </c:choose>
            
      
        </nav>

