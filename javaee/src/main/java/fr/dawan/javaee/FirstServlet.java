package fr.dawan.javaee;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class FirstServlet
 */
public class FirstServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public FirstServlet() {
        super();
    }

    // Initialisation de la servlet, Appelée une seule fois après l’instanciation de la servlet
	public void init(ServletConfig config) throws ServletException {
		System.out.println("Init Servlet");
		// Paramètre d’initialisation <init-param>
		System.out.println("annee="+config.getInitParameter("annee"));
		super.init(config);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    System.out.println("GET Servlet");
	    // paramètres d’initialisation de l’application <context-param>
	    System.out.println("Context param nom="+getServletContext().getInitParameter("nom"));
	    
	    // écriture dans la réponse
	    response.getWriter().println("<html>");
	    response.getWriter().println("<head>");
	    response.getWriter().println("<title>Hello World!</title>");
	    response.getWriter().println("</head>");
	    response.getWriter().println("<body>");
	    response.getWriter().println("<p>Hello World!</p>");
        response.getWriter().println("</body>");
	    response.getWriter().println("</html>");
	}

}
